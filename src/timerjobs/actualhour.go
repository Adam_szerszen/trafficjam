package timerjobs

import (
	"strconv"
	"time"
)

type ActualTime struct {
	hoursSingle int
	hoursDecimal int
	minutesSingle int
	minutesDecimal int
}

func (_instance *ActualTime) GetActualTime() string{
	date := ""
	date += strconv.Itoa(_instance.hoursDecimal)
	date += strconv.Itoa(_instance.hoursSingle)
	date += ":"
	date += strconv.Itoa(_instance.minutesDecimal)
	date += strconv.Itoa(_instance.minutesSingle)
	return date
}

func (_instance *ActualTime) AddMinute() {
	if _instance.minutesSingle < 9 {
		_instance.minutesSingle++
	}else {
		_instance.minutesSingle = 0
		if _instance.minutesDecimal < 5 {
			_instance.minutesDecimal++
		} else {
			_instance.minutesDecimal = 0
			if _instance.hoursDecimal < 2 && _instance.hoursSingle < 9 {
				_instance.hoursSingle++
			} else {
				if _instance.hoursDecimal < 2 {
					_instance.hoursDecimal++
					_instance.hoursSingle = 0
				} else {
					if _instance.hoursSingle < 3 {
						_instance.hoursSingle++
					} else {
						_instance.hoursSingle = 0
						_instance.hoursDecimal = 0
					}
				}
			}
		}

	}
}

func CreateActualTime() *ActualTime {
	actualTime := new(ActualTime)
	actualTime.minutesSingle = 0
	actualTime.minutesDecimal = 0
	actualTime.hoursSingle = 0
	actualTime.hoursDecimal = 0

	return actualTime
}

func ActualTimeTimerJob(actualTime *ActualTime, millisecondsPerMinute int)  {
	timeStamp := time.Duration(millisecondsPerMinute)
	for   {
		time.Sleep(time.Millisecond * timeStamp)
		actualTime.AddMinute()
	}
}