package main

import (
	"concurrency"
	"timerjobs"
	"fmt"
	"strconv"
	"time"
)

func main() {
	mapModel := concurrency.CreateMap()
	mapModel.CreateTrack(10, 10)
	mapModel.CreateTrack(12, 22)
	mapModel.ListAllStructures()
	timer := timerjobs.CreateActualTime()
	exitChannel := make (chan int)
	go timerjobs.ActualTimeTimerJob(timer, 15)
	go checkTimeSometimes(timer)
	exit := <- exitChannel
	fmt.Println("Passed value: " + strconv.Itoa(exit))
}

func checkTimeSometimes(timeCounter *timerjobs.ActualTime) {
	timeStamp := time.Duration(200)
	for   {
		time.Sleep(time.Millisecond * timeStamp)
		fmt.Println(timeCounter.GetActualTime())
	}
}