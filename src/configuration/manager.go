package configuration

import (
	"fmt"
	"helpers"
	"io/ioutil"
	"strings"
)

type Manager struct {
	Mode     string
	Speed    string
	Commands [7][2]string
}

func CreateManager() *Manager {
	instance := new(Manager)
	return instance
}

func (_instance *Manager) LoadConfigFile(path string) {
	commands, err := ioutil.ReadFile(path)
	if err != nil {
		fmt.Print(err)
	} else {
		commandLine := string(commands) // convert content to a 'string'
		splitCommand := strings.Split(commandLine, "\n")
		for i := 0; i < len(splitCommand); i++ {
			command := helpers.GetInstruction(splitCommand[i])
			_instance.Commands[i][0] = command[0]
			_instance.Commands[i][1] = command[1]
		}
	}
}

func (_instance *Manager) PrintConfigInstructions() {
	for i := 0; i < len(_instance.Commands); i++ {
		fmt.Println(_instance.Commands[i][0] + " " + _instance.Commands[i][1])
	}
}
