package interfaces

import "concurrency"

type IEnterStruct interface {
	Enter(vehicle *concurrency.Vehicle)
	GetVisitTime() int
	Leave() *concurrency.Vehicle
}