package concurrency

// zwrotnica
type Steering struct {
	// minimalny czas przestawienia
	minTime int
	VehiclesOnSteering chan *Vehicle
}

func (_instance *Steering) Enter(vehicle *Vehicle) {
	_instance.VehiclesOnSteering <- vehicle
	//TODO: log time
}

func (_instance *Steering) GetVisitTime() int {
	return _instance.minTime
}

func (_instance *Steering) Leave() *Vehicle {
	return <- _instance.VehiclesOnSteering
}

func CreateSteering(minimalTime int, availableTracks []*Track) *Steering {
	steering := new (Steering)
	steering.minTime = minimalTime
	steering.VehiclesOnSteering = make(chan *Vehicle, 1)
	return steering
}
