package concurrency

// pojazd
type Vehicle struct {
	// maksymalna prędkość
	maxSpeed int
	// maksymalna pojemność w ludziach
	maxPassengers int
	//trasa
	route [] string
	nextRouteIndex int
}

func CreateVehicle(maxSpeed int, maxPassengers int, route []string) *Vehicle {
	vehicle := new(Vehicle)
	vehicle.maxSpeed = maxSpeed
	vehicle.maxPassengers = maxPassengers
	vehicle.route = route
	vehicle.nextRouteIndex = 0
	return vehicle
}
