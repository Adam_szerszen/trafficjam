package concurrency

// tor
type Track struct {
	ID string
	length int	// tor przejazdowy. Długość toru
	maxVelocity int // tor przejazdowy. Maksymalna dopuszczalna prędkość
	VehiclesOnTrack chan *Vehicle
}

func (_instance *Track) Enter(vehicle *Vehicle)  {
	_instance.VehiclesOnTrack <- vehicle
	//TODO: log time
}

func (_instance *Track) GetVisitTime() int {

	return _instance.length * _instance.maxVelocity
}

func (_instance *Track) Leave() *Vehicle {

	return <-_instance.VehiclesOnTrack
}

func CreateTrack(length int, maxVelocity int) *Track {
	track := new(Track)
	track.length = length
	track.maxVelocity = maxVelocity
	track.VehiclesOnTrack = make(chan *Vehicle, 1)
	return track
}
