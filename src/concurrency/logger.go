package concurrency

import (
	"os"
	"strconv"
	"fmt"
)

type Log struct {
	Source string
	Message string
}

func (_instance *Log) GetLog()  string{
	return _instance.Source + " : " + _instance.Message
}

type Logger struct {
	logPath string
	currentLog chan *Log
}

func (_instance *Logger) LogEvent(source string, message string)  {
	log := new(Log)
	log.Source = source
	log.Message = message
	_instance.currentLog <- log
	_instance.logToFile(log)
	<- _instance.currentLog
}

func (_instance *Logger) logToFile(log *Log) {
	f, err := os.Create("/logs/log.txt")
	check(err)
	defer f.Close()
	n3, err := f.WriteString(log.GetLog() + "\n")
	if n3 == 0{
		fmt.Println("Wrote empty log")
	}
	f.Sync()
}

func CreateLogger(logPath string) *Logger {
	logger := new(Logger)
	logger.logPath = logPath
	logger.currentLog = make(chan *Log, 0)
	return logger
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}